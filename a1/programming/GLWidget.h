/*************************************************************************
    CSC418/2504, Winter 20l5
    Assignment 1

    Instructions:
        See main.cpp for more instructions.

        This file contains the OpenGL portion of the main window.
**************************************************************************/

#ifndef __GLWidget_h__
#define __GLWidget_h__

#include "include_gl.h"
#include "GLState.h"
#include "GLShape.h"
#include <QtOpenGL/QtOpenGL>

// Before transformed, this class displays a unit square centered at the
// origin.
class UnitSquare : public GLShape
{
public:
    using GLShape::initialize;

    void initialize(int shader_input_location)
    {
	// Use two triangles to create the square.
        GLfloat square_vertices[][2] =
        {
            { -0.5, -0.5 },  // Triangle 1
            {  0.5, -0.5 },
            {  0.5,  0.5 },
            { -0.5, -0.5 },  // Triangle 2
            {  0.5,  0.5 },
            { -0.5,  0.5 },
        };

        initialize(
	    shader_input_location,
            reinterpret_cast<const GLfloat *>(square_vertices),
            /*num_vertices=*/6,
	    GL_TRIANGLES); // Each group of three coordinates is a triangle
    }
};

// Before transformed, this class displays a unit circle centered at the
// origin.
class UnitCircle : public GLShape
{
public:
    using GLShape::initialize;

    void initialize(int shader_input_location, int num_circle_segments)
    {
        // We will draw a circle as a triangle fan.  We are careful to send
	// the second coordinate twice to properly close the circle.
        //        3     2     1
        //         +----+----+
        //        / \   |   /
        //       /   \  |  /
        //      /     \ | /
        //     /       \|/
        //   4+---------+ 0
        //        ...
        std::vector<GLfloat> circle_vertices;
        circle_vertices.push_back(0.0);
        circle_vertices.push_back(0.0);
        for (int i=0; i<=num_circle_segments; ++i)
        {
             //don't know why
            double angle = (2.1 * M_PI * i) / num_circle_segments; 
            //store in vector array the elements starting from (0,0) to (1,0) and
            //so on as the sine and cosine angles change
            circle_vertices.push_back(cos(angle));
            circle_vertices.push_back(sin(angle));
        }
        
        initialize(
	    shader_input_location,
            &circle_vertices[0],
            num_circle_segments + 1,
	    GL_TRIANGLE_FAN);
    }
};

//Penguin's face
class PenguinFace : public GLShape
{
public:
    using GLShape::initialize;

    void initialize(int shader_input_location)
    {
	// Use three triangles to create face.
        GLfloat face_vertices[][2] =
        {
            { -1, -1 },  // Triangle 1
            {  1, -1 },
            {  0.8, 0.03 },
            { -1, -1 },  // Triangle 2
            {  0.8,  0.03 },
            { -0.3,  0.15 },
            {-1,-1},     // Triangle 3
            {-0.3,0.15},
	    {-0.8,0.05},
        };

        initialize(
	    shader_input_location,
            reinterpret_cast<const GLfloat *>(face_vertices),
            /*num_vertices=*/9,
	    GL_TRIANGLES); // Each group of three coordinates is a triangle
    }
};

//Penguin's belly
class PenguinBelly : public GLShape
{
public:
    using GLShape::initialize;

    void initialize(int shader_input_location)
    {
	// Use three triangles to create face.
        GLfloat belly_vertices[][2] =
        {
            { -0.8, -0.8 },  // Triangle 1
            {  0.8, -0.8 },
            {  1.4, 2.4 },
            {- 0.8, -0.8 },  // Triangle 2
            {  1.4,  2.4 },
            {  0.8,  3.2 },
            {-0.8,-0.8},     // Triangle 3
            {0.8,3.2},
            {-0.8,3.2},
            {-0.8,-0.8},     //Triangle 4
            {-0.8,3.2},
	    {-1.4,2.4},
        };

        initialize(
	    shader_input_location,
            reinterpret_cast<const GLfloat *>(belly_vertices),
            /*num_vertices=*/12,
	    GL_TRIANGLES); // Each group of three coordinates is a triangle
    }
};


//Penguin's upper mouth
class PenguinUpperMouth : public GLShape
{
public:
    using GLShape::initialize;

    void initialize(int shader_input_location)
    {
	// Use three triangles to create upper mouth
        GLfloat umouth_vertices[][2] =
        {
            { -1, -1 },  // Triangle 1
            {  1, -1 },
            {  1, -0.7 },
            {- 1, -1 },  // Triangle 2
            {  1,  -0.7 },
            {  -1,  -0.9 },
            
        };

        initialize(
	    shader_input_location,
            reinterpret_cast<const GLfloat *>(umouth_vertices),
            /*num_vertices=*/6,
	    GL_TRIANGLES); // Each group of three coordinates is a triangle
    }
};

//Penguin's lower mouth
class PenguinLowerMouth : public GLShape
{
public:
    using GLShape::initialize;

    void initialize(int shader_input_location)
    {
	// Use three triangles to create lower mouth
        GLfloat lmouth_vertices[][2] =
        {
            { -1, -1 },  // Triangle 1
            {  1, -1 },
            {  1, -0.9 },
            {- 1, -1 },  // Triangle 2
            {  1,  -0.9 },
            {  -1,  -0.9 },
            
        };

        initialize(
	    shader_input_location,
            reinterpret_cast<const GLfloat *>(lmouth_vertices),
            /*num_vertices=*/6,
	    GL_TRIANGLES); // Each group of three coordinates is a triangle
    }
};

//Penguin's arm
class PenguinArm : public GLShape
{
public:
    using GLShape::initialize;

    void initialize(int shader_input_location)
    {
	// Use three triangles to create lower mouth
        GLfloat arm_vertices[][2] =
        {
            { -0.5, -1 },  // Triangle 1
            {  0.5, -1 },
            {  1, 2 },
            {- 0.5, -1 },  // Triangle 2
            {  1,  2 },
            {  -1,  2 },
            
        };

        initialize(
	    shader_input_location,
            reinterpret_cast<const GLfloat *>(arm_vertices),
            /*num_vertices=*/6,
	    GL_TRIANGLES); // Each group of three coordinates is a triangle
    }
};

//Penguin's leg
class PenguinLeg : public GLShape
{
public:
    using GLShape::initialize;

    void initialize(int shader_input_location)
    {
	// Use three triangles to create leg
        GLfloat leg_vertices[][2] =
        {
            { -1, -0.5 },  // Triangle 1
            {  1,-0.5 },
            {  1, 0.5 },
            {- 1, -0.5 },  // Triangle 2
            {  1, 0.5 },
            {  -1, 0.5 },
            
        };

        initialize(
	    shader_input_location,
            reinterpret_cast<const GLfloat *>(leg_vertices),
            /*num_vertices=*/6,
	    GL_TRIANGLES); // Each group of three coordinates is a triangle
    }
};

//Penguin's foot
class PenguinFoot : public GLShape
{
public:
    using GLShape::initialize;

    void initialize(int shader_input_location)
    {
	// Use three triangles to create foot
        GLfloat foot_vertices[][2] =
        {
           { -2, -1 },  // Triangle 1
            {  2,-1 },
            {  2, 0 },
            {- 2, -1 },  // Triangle 2
            {  2, 0 },
            {  -2, 0 },
            
        };

        initialize(
	    shader_input_location,
            reinterpret_cast<const GLfloat *>(foot_vertices),
            /*num_vertices=*/6,
	    GL_TRIANGLES); // Each group of three coordinates is a triangle
    }
};

class GLWidget : public QGLWidget
{
    Q_OBJECT

public:
    // These values control the bounds to display on the joint angle sliders.
    //////////////////////////////////////////////////////////////////////////
    // TODO:
    //   Add different ranges for the different joints.  Use these ranges
    //   when constructing sliders and when animating joints.
    static const double MOUTH_MIN = 0.0f;
    static const double MOUTH_MAX = 200.0f;
    static const double MOUTH_MULTIPLIER = 1000.0f;
    static const double PENGUIN_MIN = -200.0f;
    static const double PENGUIN_MAX = 200.0f;
    static const double PENGUIN_MULTIPLIER = 1000.0f;

  

    static const int 	ARM_MIN = 45;
    static const int ARM_MAX = 135;

    static const int LEFT_LEG_MIN = 45;
    static const int LEFT_LEG_MAX = 75;
    static const int LEFT_FOOT_MIN = 0;
    static const int LEFT_FOOT_MAX = 10;

    static const int RIGHT_LEG_MIN=45;
    static const int RIGHT_LEG_MAX = 75;
    static const double RIGHT_FOOT_MIN = 0;
    static const double RIGHT_FOOT_MAX = 10;

    static const double LEG_TRANSLATE_MIN = -20.0f;
    static const double LEG_TRANSLATE_MAX = 180.0f;
    static const double LEG_TRANSLATE_MULTIPLIER = 40.0f;

    //////////////////////////////////////////////////////////////////////////
    static const int JOINT_MIN = 75;
    static const int JOINT_MAX = 105;

    GLWidget(QWidget *parent=NULL);

public slots:
    // This method is called when the user changes the joint angle slider.
    //////////////////////////////////////////////////////////////////////////
    // TODO:
    //   There is currently only one joint, but you need to add more.
     void setMouthTranslate(int translate)
    {
        // This method is called when the user changes the slider value.
        m_penguin_mouth = translate/MOUTH_MULTIPLIER;
      
        // Call update() to trigger a redraw.
        update();
    }
    void setArmAngle(int angle)
    {
        // This method is called when the user changes the slider value.
        m_arm_angle = angle;
         
        // Call update() to trigger a redraw.
        update();
    }


    void setLeftLegAngle(int angle)
    {
        // This method is called when the user changes the slider value.
        m_left_leg_angle = angle;
         
        // Call update() to trigger a redraw.
        update();
    }

    void setLeftFootAngle(int angle)
    {
        // This method is called when the user changes the slider value.
        m_left_foot_angle = angle;
         
        // Call update() to trigger a redraw.
        update();
    }

    void setRightLegAngle(int angle)
    {
        // This method is called when the user changes the slider value.
        m_right_leg_angle = angle;
         
        // Call update() to trigger a redraw.
        update();
    }

   void setRightFootAngle(int angle)
    {
        // This method is called when the user changes the slider value.
        m_right_foot_angle = angle ;
         
        // Call update() to trigger a redraw.
        update();
    }

 void setLegTranslate(int translate)
    {
        // This method is called when the user changes the slider value.
        m_leg_translate = translate/LEG_TRANSLATE_MULTIPLIER;
        // Call update() to trigger a redraw.
        update();
    }

    //////////////////////////////////////////////////////////////////////////
    void setJointAngle(int angle)
    {
        // This method is called when the user changes the slider value.
        m_joint_angle = angle;
         
        // Call update() to trigger a redraw.
        update();
    }

    void onPressAnimate(int is_animating)
    {
        // This method is called when the user changes the animation checkbox.
        m_is_animating = (bool)is_animating;
        m_animation_frame = 0;
        update();
    }

protected:
    void initializeGL();
    void resizeGL(int width, int height);
    void paintGL();
    void timerEvent(QTimerEvent *event);

private:
    GLTransformStack &transformStack()
    { return m_gl_state.transformStack(); }

private:
    GLState m_gl_state;
    bool m_is_animating;
    int m_animation_frame;
    UnitSquare m_unit_square;
    UnitCircle m_unit_circle;
    //////////////////////////////////////////////////////////////////////////
    // TODO: Add additional joint parameters.
    PenguinFace m_penguin_face;
    PenguinBelly m_penguin_belly;
    PenguinUpperMouth m_penguin_upper_mouth;
    PenguinLowerMouth m_penguin_lower_mouth;
    PenguinArm  m_penguin_arm;
    PenguinLeg  m_penguin_leg;
    PenguinFoot m_penguin_foot;
    //////////////////////////////////////////////////////////////////////////
    double m_joint_angle;
    double m_arm_angle;
    double m_left_leg_angle;
    double m_left_foot_angle;
    double m_right_leg_angle;
    double m_right_foot_angle;
    double m_penguin_mouth;
    double m_leg_translate;
    double m_whole_penguin_translate;
};

#endif
